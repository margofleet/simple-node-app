# Base image
FROM node

# Install OS dependencies
RUN apt-get update && apt-get install -y \
    git \
    zip \
    unzip \
    vim \
    curl

# Changing working directory
WORKDIR /usr/application

# Copying local code files to docker container path
COPY application/index.js .
COPY application/package.json .
COPY start.sh .

# Run the application
ENTRYPOINT [ "/bin/bash", "start.sh" ]