const http = require('http');
var url = require('url');
 
const arguments = process.argv.slice(2);
const component = arguments[0];
const port = arguments[1];
 
const requestHandler = (request, response) => {
  console.log(`requested ${component}: ${request.url}`);
 
  var parsedUrl = url.parse(request.url, true);
  var pathName = parsedUrl.pathname
 
  if(pathName == "/api"){
    http.get('http://localhost:3001', (resp) => {
      let data = '';
    
      resp.on('data', (chunk) => {
        data += chunk;
      });
    
      resp.on('end', () => {
        response.end(data);
      });
    
    }).on("error", (err) => {
      response.end("Error: " + err.message);
    });
  }
  else{
    response.end(`Hello ${component}!`);
  }
}
 
const server = http.createServer(requestHandler)
 
server.listen(port, (err) => {
 
  if (err) {
    return console.log('something bad happened', err);
  }
 
  console.log(`${component} is listening on port ${port}`);
})
