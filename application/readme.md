# Web application
This is a conceptual nodejs application which consists of a frontend and an api. To keep things simple both servers are handled by the same js file, which uses two arguments: the application name and the port. For example, the server can start the frontend on port 3000 like this: `node index.js frontend 3000`

## Requirements
 * nodejs
 * npm


## Running the application
* To start the frontend: `npm run start:fe`
* To start the backend: `npm run start:be`
* Your running servers will be http://localhost:3000 and http://localhost:3001
* The application replies to requests on any path, including /health