# Node.js simple web application

This simple Node.js application is composed of two components: frontend and backend (api). In this project the main focus is the infrastructure, therefore these are the technologies used to accomplish IAAC, containerization per components, CI/CD:

-   **Terraform** to provision the infrastructure on AWS cloud provider.
-   **Amazon EKS** to create a Kubernetes cluster for containerization.
-   **.gitlab-ci.yml** file used to create a pipeline that  will automatically deploy the application when new code is pushed in Git.

### Requirements

Before running the terraform config files, there are some manual steps that need to be performed:

-   Install [***terraform***](https://learn.hashicorp.com/terraform/getting-started/install.html).

-   Create an AWS S3 bucket (unique bucket name) that needs to have a policy similar with this one:

~~~
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<AWS_ACCCOUNT_ID>:user/ana"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::state-bucket-for-tf"
        },
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<AWS_ACCCOUNT_ID>:user/ana"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Resource": "arn:aws:s3:::state-bucket-for-tf/development/terraform.tfstate"
        }
    ]
}
~~~

> *Note! This bucket is necessary for saving the terraform state file in cloud, instead of saving it locally. If you enable versioning on the bucket, you will have a backup of the terraform state file before any change.*


-   Create a AWS Dynamo DB table from AWS console.

![alt text](images/dynamo.png "DynamoDB table")

> *Note! The DynamoDB table is needed to lock the terraform state file for being used at the same time by multiple users. Both S3 bucket and DynamoDB table are needed for initialization of terraform backend config file.*

-   Rename the ***local.auto.tfvars.example*** file to ***local.auto.tfvars*** and replace the access key and secret with the values from your programatic user.

Before accessing the kubenetes cluster, install [***kubectl***](https://kubernetes.io/docs/tasks/tools/install-kubectl/) and [***helm3***](https://helm.sh/docs/intro/install/).

### Deploying the infrastructure

Terraform will create an AWS EKS cluster and one worker node group where the application will run.

From the terminal, go to the terraform directory:
~~~
cd ./terraform
~~~

Initialize terraform:

~~~
terraform init
~~~

Print the execution plan:

~~~
terraform plan
~~~

Apply the changes, so that the resources will be created:

~~~
terraform apply
~~~


**Additional notes:**
> The cluster can be easily stopped and started from terraform with these commands:

~~~
terraform destroy
~~~

~~~
terraform apply
~~~


### Building the application

To upload the docker image to the Gitlab repository, run the following commands:

~~~
docker login registry.gitlab.com
~~~

~~~
docker build -t registry.gitlab.com/margofleet/simple-node-app .
~~~

~~~
docker push registry.gitlab.com/margofleet/simple-node-app
~~~

We can version the docker images by tags inside the Gitlab repository, example:

~~~
docker build -t registry.gitlab.com/margofleet/simple-node-app:1.2 .
~~~

~~~
docker push registry.gitlab.com/margofleet/simple-node-app:1.2
~~~

When a new version of the docker image was built we need to modify the k8s deployment to reflect the new image tag.


### Deploying the application

To deploy the application, run:

~~~
kubectl apply -f ./k8s/deployment/deployment.yaml
~~~

Deploy the service so we can access the application:

~~~
kubectl apply -f ./k8s/deployment/service.yaml
~~~

Port forward the service ports to localhost ports:

~~~
kubectl port-forward service/hello-world 3000 3001
~~~

Or, you can port forward the deployment's ports:

~~~
kubectl port-forward deployment/hello-world-deployment 3000 3001
~~~

The application is now accessible from local pc browser at these URLs:

- frontend -> http://localhost:3000
- backend -> http://localhost:3001

The frontend and backend are communicating between each other inside the pod, this functionality can be observed by viewing the content of the backend when accessing the frontend URL:

> http://localhost:3000/api


### CI/CD configuration

The CI/CD from Gitlab was used to create the automation for future deployments. From project -> Settings -> CI/CD -> Runners expand -> copy the token.

After we get the token, we need to modify the gitlab runner helm values at: `./k8s/gitlab/values.yaml`.

Next, the following commands were issued to add the gitlab helm repo, create the namespace in kubernetes and finally, install the runner on the containers:

~~~
cd ./k8s/gitlab/
~~~

~~~
helm repo add gitlab https://charts.gitlab.io
~~~

~~~
kubectl create namespace gitlab-runners
~~~

~~~
helm install --namespace gitlab-runners gitlab-runner -f values.yaml gitlab/gitlab-runner
~~~

Every time when the code is updated and pushed to git, the gitlab-ci will automatically build and deploy the new changes for the application.

### Additional notes

A bash script was created that waits for the application to start. The script has two parameters APP_COMPONENT and APP_PORT. This is how you can run it:

~~~
chmod +x ./wait.sh
~~~

~~~
./wait.sh frontend 3000
or
./wait.sh backend 3001
~~~

### Future improvements

-   Implement a NAT gateway in terraform configuration for worker nodes to hone the security of the system.
