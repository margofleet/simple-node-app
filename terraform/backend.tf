##################################################################################
# BACKEND
##################################################################################

terraform {
  backend "s3" {
    bucket         = "state-bucket-for-tf"
    key            = "develop/terraform.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-state-lock-dynamo"
    profile        = "default"
  }
}
