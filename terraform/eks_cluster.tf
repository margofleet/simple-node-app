##################################################################################
# RESOURCES
##################################################################################

# SECURITY GROUPS

resource "aws_security_group" "cluster_sg_access" {
    name        =   "cluster_sg"
    description =   "Cluster communication with worker nodes"
    vpc_id      =   aws_vpc.my_vpc.id

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
}

# ROLES

resource "aws_iam_role" "eks_cluster" {
    name                =   "eks-cluster"

    assume_role_policy  =   <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]   
}
POLICY
}

# ROLE ATTACHMENTS

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicyAttach" {
    policy_arn  =   "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
    role        =   aws_iam_role.eks_cluster.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicyAttach" {
    policy_arn  =   "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
    role        =   aws_iam_role.eks_cluster.name
}

# CLUSTER

resource "aws_eks_cluster" "tf_eks_cluster"    {
    version     =   "1.17"
    name        =   var.cluster_name
    role_arn    =   aws_iam_role.eks_cluster.arn

    vpc_config  { 
        subnet_ids          =   aws_subnet.subnet[*].id
        security_group_ids  =   [aws_security_group.cluster_sg_access.id]
    }

    depends_on  =   [
        aws_iam_role_policy_attachment.AmazonEKSClusterPolicyAttach,
        aws_iam_role_policy_attachment.AmazonEKSServicePolicyAttach
    ]
}

locals {

  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    server: ${aws_eks_cluster.tf_eks_cluster.endpoint}
    certificate-authority-data: ${aws_eks_cluster.tf_eks_cluster.certificate_authority.0.data}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${var.cluster_name}"
KUBECONFIG
}

resource "local_file" "kubeconfig" {
  content  = local.kubeconfig
  filename = pathexpand(var.kubeconfig_path)
}