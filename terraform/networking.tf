##################################################################################
# RESOURCES
##################################################################################

# NETWORKING

resource "aws_vpc" "my_vpc" {
  cidr_block            = "10.0.0.0/16"
  enable_dns_hostnames  =   true

  tags        = map(
    "Name", "terraform-eks-node",
    "kubernetes.io/cluster/${var.cluster_name}", "shared"
  )
}

resource "aws_internet_gateway" "my_igw" {
  vpc_id  = aws_vpc.my_vpc.id

  tags    = {
    Name  = "Assignment tf IGW"
  }
}

resource "aws_subnet" "subnet" {
  count                   = 2

  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "10.0.${count.index}.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.my_vpc.id

  tags  = map(
    "Name", "terraform-eks-node",
    "kubernetes.io/cluster/${var.cluster_name}", "shared"
  )
}

# ROUTING

resource "aws_route_table" "my_rt" {
  vpc_id  = aws_vpc.my_vpc.id

  route {
    cidr_block  = "0.0.0.0/0"
    gateway_id  = aws_internet_gateway.my_igw.id
  }

  tags    = {
    Name  = "Assignment tf RT"
  }
}

resource "aws_route_table_association" "my_sub_rt_assoc" {
    count           =   2
    subnet_id       =   aws_subnet.subnet.*.id[count.index]
    route_table_id  =   aws_route_table.my_rt.id
}
