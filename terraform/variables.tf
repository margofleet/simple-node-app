##################################################################################
# VARIABLES
##################################################################################

variable "aws_access_key" {
    description = ""
    default = ""  
}

variable "aws_secret_key" {
    description = ""
    default = ""  
}
variable "aws_region" {
    default = "eu-central-1"  
}

variable "cluster_name" {
    default =   "tf-eks-cluster"
    type    =   string
}

variable "kubeconfig_path" {
  description = "The path to save the kubeconfig to"
  default     = "~/.kube/config"
}

