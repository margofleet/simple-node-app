#!/bin/bash

# APP_COMPONENT 
# APP_PORT

APP_COMPONENT=$1
APP_PORT=$2


until $(curl --output /dev/null --silent --head --fail http://localhost:$APP_PORT); do
    printf "Waiting for the $APP_COMPONENT application to be up...\n"

    sleep 2
done

printf "\nThe $APP_COMPONENT application is up.\n"

